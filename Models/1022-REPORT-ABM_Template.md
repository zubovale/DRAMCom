In order to successfully complete this assignment you need to commit this report to your project git repository on or before **11:59pm on Friday October 22**.

# <center>Using Agent Based Models in Microbial Modeling</center>

<center>by Alexander Zubov</center>

---
# Instructions


Write a ~1 (or 2) page report about how Agent Based Models (ABMs) could be used in **your area of research**. If you do not have an area of research set, write about how Graph Theory could be used in **your project**.

- Summarize what you learned about ABM.

There are three basic forms to this report:

1. ABM is already used in your area of research.  Provide a summary and include references.  If possible include a short example that could be demonstrated in class.

2. Describe how you could incorporate ABM into your research domain.  What research questions might you be able to ask, improve or validate?

3. ABM cannot be used in your area of research.  Explain in detail why they can't be used.  

To be clear, I think 1 and 2 will be the most common reports.  If you choose 3 you need to make a very convincing argument.  Use your imaginations!  The best idea come from combining expertise in seemingly unrelated topics. 

---



The past classes dealth with the topic of Agent-Based Modeling (ABM). ABM itself is very much intertwined with the general concept of Object-Oriented Programming (OOP). Python is a programming language designed with the concept of OOP in mind. OOP gives Python the large flexibility which gave rise to its high popularity.  
In essence, OOP describes the usage of a class that can be easily reused to generate objects from it. A class is defined by certain properties, like variables, and further contains methods that can be applied to any object from that class. Any objects instantiated from that class will have the variables and applicabe methods that are defined by that class. This allows one to quickly create a large amount of similar, yet individual objects from such a class.  
  
The reason OOP is so convenient for ABM is that ABM deals with the simulation of a large amount of individual so-called agents. Every agent has its own attributes and is viewed as an individual entitiy inside the simulation space. The collective amount of all agents can give rise to specific phenomena that would not be possible with one agent alone.  
An example for ABM used in the class is ant foraging. In fact, there is a whole type of simulations called ant colony optimization problems that deal with problems similar to real ants on the search for food. Using real food foraging as an example, the ABM model created throughout the past classes simulated ants on a two-dimensional world searching for food. Upon encountering food, ants pick up a piece and head back home. Thereby the ants lay down a trail of pheromones that signal other ants nearby the source of the food. Ants nearby will follow the maximal intensity of the trail and simultaneously increase the pheromone concentration. This way, an increasing gradient coming from the food source will develop and more and more ants will follow it to collect the food in this location. After a while, the behavior of the ants will switch from random searching to a directed movement towards the source of food and back to the colony. This is what is meant with specific phenomena that are only observable from an agent collective, that is however initiated by only few - or even just one - individual agents, and thus shows the power ABM systems have in describing systematic behavior.  
  
  
In the field of Quanititative and Systems Biology, AGM's have been recently established as an emerging tool to further enhance our comprehensive understanding of systemic phenomena below, at and above the scale of cells <sup>[1], [2], [3]</sup>.  
The paper by Soheilypour & Mofrad (2018) presents several applictions of AGM in mostly subcellular modeling, like the simulation of RNA or protein movements inside the cell (compartments) as well as their interactions with each other that influence the movement <sup>[2]</sup>. Most of these models rely predominantly on diffusion dynamics and the effects certain spatial and/or quantitative parameters have on such movements - for example the concentration of certain protein factors that determines the rate of mRNA translocation across the nuclear pores <sup>[2]</sup>.  
Other approaches focus on the interactions between whole cells. Yu & Bagheri (2020) presented an approach to study the interactions of differing cells in a complex heterogenous tissue <sup>[1]</sup>. Their model included cell growth and metabolism, as well as cell-cell communication and other processes to study the spatiotemporal outcome of their *in silico* cell consortium.  
  
Related to the semester project, AGM can also be coupled with pure metabolic simulations like FBA. Harcombe et al. (2014) developed a Python package called "COMETS", standing for "Computation of Microbial Ecosystems in Time and Space" <sup>[3]</sup>. This package applies the method of dynamic Flux Balance Analysis (dFBA) on individual microbial cells, which can be different species/strains/etc. The dynamic rate-of-change of growth and other metabolic functions is simulated alongside simple diffusion mechanics that allow a simple, yet relatively realistic approximation of something like the growth of cultures on a petri dish - in fact, COMETS allows one to construct a virtual petri dish and model the growth of organisms on it. One particular aspect lying in the field-of-interest of the author here could be the co-culturing of metabolically dependent species, like for example cyanobacteria (that "eat" CO<sub>2</sub> and produce sugar) and heterotrophic bacteria (that eat sugar and respire it to CO<sub>2</sub>). An outcome of this simulation in nutrient-poor media could be that only agents that are close to their partner will survive, else they would not be able to get the necessary metabolites. 
While COMETS is surely a package with refined code that is functioning and also performing robustly, it is not impossible to implement an own, much simpler version of it and apply it to the project dealing with dynamic Ressource Balance Analysis (dRBA). As RBA is able to implicitely simulate the content of macromolecules - or at least enzymes - this would allow to enhance the interaction by including export and possibly exchange of proteins or alike. In a more general way though, ABM would enhance microbial modeling by allowing it to display observations as described in the presentation of COMETS, which would for example entail the close aggregation of metabolically dependent species. Such observations will further drive our understanding of such complex systems, possibly even up to ecosystem scale.

---
# References

[1] Yu, J. S., & Bagheri, N. (2020). Agent-based models predict emergent behavior of heterogeneous cell populations in dynamic microenvironments. Frontiers in bioengineering and biotechnology, 8, 249.  
[2] Soheilypour, M., & Mofrad, M. R. (2018). Agent‐based modeling in molecular systems biology. BioEssays, 40(7), 1800020.  
[3] Harcombe, W. R., Riehl, W. J., Dukovski, I., Granger, B. R., Betts, A., Lang, A. H., ... & Segre, D. (2014). Metabolic resource allocation in individual microbes determines ecosystem interactions and spatial dynamics. Cell reports, 7(4), 1104-1115.  

-----
### Congratulations, you are done!

Now, you just need to commit and push this report to your project git repository. 
