In order to successfully complete this assignment you need to commit this report to your project git repository on or before **11:59pm on Friday November 5**.

# <center>Using Machine Learning (ML) in Microbial Modeling</center>

<center>by Alexander Zubov</center>


---
# Instructions


Write a ~1 (or 2) page report about how machine learning (ML) models could be used in **your area of research**. If you do not have an area of research set, write about how machine learning could be used in **your project**.

- Summarize what you learned about ML.

There are three basic forms to this report:

1. ML is already used in your area of research.  Provide a summary and include references.  If possible include a short example that could be demonstrated in class.

2. Describe how you could incorporate ML into your research domain.  What research questions might you be able to ask, improve or validate?

3. ML cannot be used in your area of research.  Explain in detail why they can't be used.  

To be clear, I think 1 and 2 will be the most common reports.  If you choose 3 you need to make a very convincing argument.  Use your imaginations!  The best idea come from combining expertise in seemingly unrelated topics. 

---



A large and very relevant topic being the focus in the past lectures is Machine Learning (ML). The basic principle behind ML is quite simple: ML describes the process of training parameters in a certain solving algorithm such that this algorithm can then accurately predict results of an input using those fine-tuned parameters.  
ML algorithms can differ in both the way they are trained and the predictions they make. Supervised Learning describes the labelling of input data such that an ML algorithm "knows" what it tries to predict. On the other hand, unsupervised algorithms can derive "their own" conclusions about the data without the experimentators bias. Further, ML algorithms can either be classification models or regression models. The former predicts categorical data, the latter continuous one (although regression models can also be used in the prediction of categorical data).  

There are a multitude of different ML algorithms available and used. One of the simplest ones is Support Vector Machine (SVM) that tries to divide data into different classes with a large enough/significant distance. The method was introduced by using it on a dataset about benign and malign breast cancer - the two types to be classified/predicted. There, more core principles were mentioned: for any given dataset one wants to train an ML algorithm on, one divides it into a training set used to fine-tune the algorithmic parameters and a testing set to assess the accuracy of predictions of the algorithm. As a general rule of thumb, the training set is 75 % of the total data set and the rest is used for testing.  

The majority of the classes however focused on possibly the most abundantly used, almost buzz-word'y model of Artificial Neural Networks (ANN). As their name says, ANN's mimick the natural network of neurons inside the brain by utilizing multiple nodes for data input, processing and output. An ANN has one output layer of nodes and one input layer, as well as at least one so-called hidden layer - the layer containing the processing nodes. The (first) hidden layer takes input data multiplied with parameterized weights and uses an activation function to produce an intermediary output. In multi-layered ANN this output is then processed by the next layer and so forth, until it is multiplied with another set of weights to produce the final output by the output layer. The internal weights on the connecting edges between and the activation function parameters represent the parameters to-be-trained. For actual training however one does not use the direction of data as described, which would be forward propagation, but insted only initializes the model via forward propagation and then trains it using backward propagation. There, the output is already known and one tries to find weight values that produce an accurate estimation of the actual results. The accuracy is hereby measured using a cost function representing the error - as such, the goal is to minimize the error. Even though not too difficult as desribed just like that, the procedure poses a problem for increasing numbers of weights. The so-called Curse of Dimensionality describes the problem that the amount of optimizations (minimizations for cost/error) increases exponentially with the number of weights. To avoid testing all possible combinations of weights instead, a commonly used procedure is an analogue to gradient descent: initially, weight values close to the starting points are tested and it is proceeded in the direction of decreasing cost values.  
With this, the only thing left to do is to play around with the amount of hidden layers, connections and types of activation functions to increase the accuracy of the ANN.  

There exist multiple ways to implement ML algorithms in python. The most basic one is ScikitLearn, which is however not computationally efficient. A much more powerful method that focuses on ANN is TensorFlow developed by Google. With TensorFlow one can build, train and test ANN's with relative ease and utilizing the C/C++ backend of the core TensorFlow Suite the computations are highly opzimized. Having a GPU or even a dedicated Tensor Processing Unit (TPU) greatly enhances the computation speed of an ANN (or any ML algorithm in the case of GPU's). Instead of relying on own hardware however, Google offers the cloud-based application called Colaboratory where one can run for example Python code using CPU, GPU or TPU, making it an attractive tool for any beginners or private hobbyists in the field of ML and ANN.  
  

Machine Learning has recently been integrated into the field of Biology, or specifically constraint-based modeling (CBM) of microbial metabolism <sup>[1], [2]</sup>. Both CBM and ML can serve as foundations of one another, thus increasing the overall performance of the whole model. For example, ML methods can help in enhancing the model creation process from gene annotations and further help in filling gaps in the model network <sup>[1], [2]</sup>. The construction of a metabolic model is not trivial and even after automated initial generation from a genome such a model requires extensive manual curation to make it viable. Furthermore ML algorithms can help in generating senseful constraints, i.e. flux bounds for the model <sup>[1]</sup>. On the other side, the CBM can serve as input data for an ML algorithm that can then derive and predict certain reaction fluxes or growth rates <sup>[1]</sup>. Also, kinetic parameters for other types of models or extensions of CBM's like dynamic FBA (dFBA) can be fitted using ML algorithms and CBM outputs as inputs for the algorithm <sup>[1]</sup>. The synergy between these two methods can thus result in an a model construction that is - at least in principal - optimized to create a realistic simulation of microbial metabolism.

---
# References

[1] Rana, P., Berry, C., Ghosh, P., & Fong, S. S. (2020). Recent advances on constraint-based models by integrating machine learning. Current opinion in biotechnology, 64, 85-91.  
[2] Khaleghi, M. K., Savizi, I. S. P., Lewis, N. E., & Shojaosadati, S. A. (2021). Synergisms of machine learning and constraint‐based modeling of metabolism for analysis and optimization of fermentation parameters. Biotechnology Journal, 2100212.

-----

### Congratulations, you are done!

Now, you just need to commit and push this report to your project git repository. 
