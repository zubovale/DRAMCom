In order to successfully complete this assignment you need to commit this report to your project git repository on or before **11:59pm on Friday October 8**.

# <center>Using Graph Theory in Microbial Modeling</center>

<center>by Alexander Zubov</center>

---
# Instructions

Write a ~1 (or 2) page report about how Graph Theory could be used in **your area of research**. If you do not have an area of research set, write about how Graph Theory could be used in **your project**.

- Summarize what you learned about Graph Theory.

There are three basic forms to this report:

1. Graph Theory is already used in your area of research.  Provide a summary and include references.  If possible include a short example that could be demonstrated in class.

2. Describe how you could incorporate Graph Theory into your research domain.  What research questions might you be able to ask, improve or validate?

3. Graph Theory can not be used in your area of research.  Explain in detail why they can't be used.  

To be clear, I think 1 and 2 will be the most common reports.  If you choose 3 you need to make a very convincing argument.  Use your imaginations!  The best idea come from combining expertise in seemingly unrelated topics. 

---


In the previous class segment the topic of graph theory was discussed. Graph theory describes the mathematical abstraction of certain things with connections as so called graphs. These could include points in a city seperated by rivers with bridges as connections for example, as first discribed by famous mathematician Leonhard Euler about Koenigsberg (today Kaliningrad).  
In graph theory, points-of-interest are called nodes and are connected via so-called edges. A graph is then the set of all nodes and edges and thus describes a problem as a network. Edges can have certain values assigned to them with the meaning dependent on the particular context, for example for spatial problems the most intuitive meaning of the edge-value is the distance between certain points. While visualizing a graph as a network with connected points is the most intuituve way to depict a graph, it does not represent the only possible one. A more abstract yet equivalent way of depicting a graph is via a so-called adjacency matrix. This 2-dimensional matrix has the size <a href="https://www.codecogs.com/eqnedit.php?latex=N&space;\times&space;N" target="_blank"><img src="https://latex.codecogs.com/png.latex?N&space;\times&space;N" title="N \times N" /></a> with <a href="https://www.codecogs.com/eqnedit.php?latex=N" target="_blank"><img src="https://latex.codecogs.com/png.latex?N" title="N" /></a> equaling the number of nodes in the graph. In each row and column, a number - the edge value - corresponds to a connection between two nodes. As a result, the trace of the matrix is zero as a node cannot be connected to itself.  

Graph theory problems can be implemented in Python with the package networkx. In networkx, one instantiates a graph class and then add nodes and edges to it. These can be added via different methods, either manually or iteratively from scratch, or also from some data including an adjacency matrix. The graph can then also be visualized as an adjacency matrix, like via a heatmap with the `pyplot.spy()` function, or as a network. Different methods are available as well for the network visualization, where de default `draw()` command will draw an optimized random network. One can also define the layout of the network, for example `.circular_layout()` will result in the nodes being on the edge of a circle/ellipse. The possibilities do not end there as more options for graph analysis are available in networkx. Another problem is the colouring of graph nodes, where one would wish for no same colours being connected with each other via an edge. This is called greedy colouring, as the least amount of colour is used - typically a maximum of four colours should suffice. Other greedy algorithms for other problems also exist, like greedy modularity maximization for detection of potential subnetworks called modules within a network/graph.  

These principles were taught using a variety of different examples. Example networks like the relationships of people within the Zachary Karate Club were used to illustrate the different characteristics of graphs and the methods that can be used on them. Two major examples stand out: one dealing with web-scraping and another one focusing more on group-work but combining that with the application of graph theory on cellphone towers.  
Web-scraping describes the process of iteratively searching for links on webpages and travelling further from those links to new webpages. Among the information gathered via this method is the connectivity of different webpages with each other, which can be easily depicted using a graph. Concretely MSU webpages were used in this class assignment. Web-scraping can be used for different purposes as well, but those are not discussed here further as they diverge from the topic at hand.  
The second example uses the problem that cellphone companies often experience: since cellphone towers with overlapping regions can cause interference when using the same frequency, they should use different frequencies for signalling. However, using many different frequencies is uneconomic as it is costly, hence companies would like to simultaneously minimize both signal interference and frequency usage. This problem can be converted into a graph with the celltowers as nodes and their distance from each other as edges. In the example assignment, colours were used as different frequencies. The above mentioned method of greedy colouring was empployed to minimize the usage of different colours on celltowers that have been deemed to be close enough to each other for interference. While quite simplified, the examples showed how connectivity in various forms can be translated into a mathematical problem and analyzed with a standardized method. 
  
  
In biology, graph theory can be applied in many ways. Neural networks are probably among the first networks that come to mind that can easily be depicted as a graph, but also microbial and molecular biological fields can benefit from graph theory. Any cell of any organism is a collection of a multitude of different networks: interactions between different proteins or proteins and genes can be depicted as graphs <sup>[1]</sup>. Nodes would be proteins (and/or genes) and edges the interactions between them. One possible attribute of graphs is that the edges can either be undirected or directed: meaning, in an undirected edge a connection is established from one node to the other and back, but in a directed edge this connection goes only from one node to another. Such directed graph are also used predominantly for signal transduction networks and metabolic networks <sup>[1]</sup>. Their, the directionality is important: in signal networks this depicts a directed signal cascade which obviously only travels in one direction (in most cases); in metabolic networks many reactions are irreversible and therefore cannot happen in both sides of a chemical equation. Many bioinformatic databases utilize these networks for data storage and presentation which helps scientists to both find and understand the connections between certain elements. These include BLAST for gene relationships <sup>[2]</sup> and KEGG for signal transduction and metabolic networks <sup>[3]</sup>.  

In the course project at hand, graph theory is not the main focus and can further not really be incorporated in a senseful manner. The focus is instead on the dynamic simulation of the metabolism and ressource allocation of microbal organisms. While metabolic networks can be depicted as graphs, as mentioned above, the underlying methodology of Flux Balance Analysis (FBA) <sup>[4], [5]</sup> and Ressource Balance Analysis (RBA) <sup>[6]</sup> does not use graph theory algorithms but linear programming to calculate reaction flux distributions. The stoichiometric matrix <a href="https://www.codecogs.com/eqnedit.php?latex=S" target="_blank"><img src="https://latex.codecogs.com/png.latex?S" title="S" /></a> used in this algorithm is an <a href="https://www.codecogs.com/eqnedit.php?latex=m&space;\times&space;n" target="_blank"><img src="https://latex.codecogs.com/png.latex?m&space;\times&space;n" title="m \times n" /></a> matrix with <a href="https://www.codecogs.com/eqnedit.php?latex=m" target="_blank"><img src="https://latex.codecogs.com/png.latex?m" title="m" /></a> metabolites and <a href="https://www.codecogs.com/eqnedit.php?latex=n" target="_blank"><img src="https://latex.codecogs.com/png.latex?n" title="n" /></a> reactions. Values indicate the occurence of a metabolite in a reaction and on which side of the chemical equation it stands. This is not similar to an adjacency matrix, however it can certainly be converted into one when viewing what metabolites occur in what reaction and thus how they are connected. In theory, since there may exist several differing metabolic pathways for the production/degradation/etc. of certain compounds one could argue that finding the "shortest" one is desired to reduce the enzyme cost, as bigger pathways consume more enzymes and thus ressources - one of the main assumptions of (parsimonious) RBA. The "length" could then further be calculated as an abstracted value based on cost of ATP (the energy currency of the cell) or other cofactors, turnover time of the enzymes or possible flux as measures of the velocity of the conversion, and other phenomena. Whether or not this provides a valid option for analyzing microbial metabolism remains unanswered, but this approach is certainly limited as it would only find the most optimal pathways and not much mure, while RBA is able to calculate and incorporate a significantly bigger amount of methods and results and the previously mentioned assumptions are already (directly or implicitely) implemented into the algorithm. Therefore, graph theory is probably not much applicable to the specific research topic, even though it has its attractive characteristics.

---
# References

In all three example provide some references to papers you found that help illustrate your argument. I prefer references with links to papers.  


[1] Koutrouli, M., Karatzas, E., Paez-Espino, D., & Pavlopoulos, G. A. (2020). A guide to conquer the biological network era using graph theory. Frontiers in bioengineering and biotechnology, 8, 34.  
[2] NCBI Resource Coordinators (2018). Database resources of the National Center for Biotechnology Information. Nucleic acids research, 46(D1), D8–D13.  
Direct Link: https://blast.ncbi.nlm.nih.gov/Blast.cgi  
[3] Kanehisa, M., Sato, Y., Kawashima, M., Furumichi, M., & Tanabe, M. (2016). KEGG as a reference resource for gene and protein annotation. Nucleic acids research, 44(D1), D457-D462.  
Direct Link: https://www.genome.jp/kegg/kegg1.html
[4] Feist, A. M., & Palsson, B. Ø. (2008). The growing scope of applications of genome-scale metabolic reconstructions using Escherichia coli. Nature biotechnology, 26(6), 659-667.  
[5] Lewis, N. E., Nagarajan, H., & Palsson, B. O. (2012). Constraining the metabolic genotype–phenotype relationship using a phylogeny of in silico methods. Nature Reviews Microbiology, 10(4), 291-305.  
[6] Goelzer, A., Muntel, J., Chubukov, V., Jules, M., Prestel, E., Nölker, R., ... & Fromion, V. (2015). Quantitative prediction of genome-wide resource allocation in bacteria. Metabolic engineering, 32, 232-243.

-----
### Congratulations, you are done!

Now, you just need to submit this report by uploading it to the course <a href="https://d2l.msu.edu/">Desire2Learn</a> web page for today's dropbox.
