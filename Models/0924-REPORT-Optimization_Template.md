In order to successfully complete this assignment you need to turn in a project proposal to D2L on or before **11:59pm on Friday September 24**.

# <center>Using Optimization in Microbial Modeling</center>

<center>by Alexander Zubov</center>

---
# Instructions

Write a ~1 (or 2) page report about how Optimization methods could be used in **your area of research**. If you do not have an area of research set, write about how Optimzation could be used in **your project**.

- Summarize what you learned in the optimization methods.

There are three basic forms to this report:

1. Optimization methods are already used in your area of research.  Provide a summary and include references.  If possible include a short example that could be demonstrated in class.

2. Describe how you could incorporate Optimization methods into your research domain.  What research questions might you be able to ask, improve or validate?

3. Optimization methods can not be used in your area of research.  Explain in detail why they can't be used.  

To be clear, I think 1 and 2 will be the most common reports.  If you choose 3 you need to make a very convincing argument.  Use your imaginations!  The best idea come from combining expertise in seemingly unrelated topics. 

Write the report so someone outside of the course could make sense of how Optimization methods can be used in your area of research. 

---

The previous classes dealt with the topic of optimization in programming. The two main fields were introduced: the computational science-oriented field of optimizing a mathematical formulation, and the computer science-oriented field of optimizing an algorithm.   

The main content focused on optimization as an applied mathematical concept. In short, optimization describes the maximization or minimization of a certain function under certain constraints. This function is called the objective function, and the constraints restrict the possible solution space of the function. Vividly speaking optimization is about finding the smallest or largest value (on the graph) of a mathematical function, like a parabola – though, in computational sciences, the objective function can be rarely visualized that easily.  
The same can be said about the actual procedure of an optimization: while analytical optimization is possible and required for rigorously conducting mathematical proofs, it is prone to fail or be too difficult to accomplish for any science-related problem. More so, an exact solution is not necessarily required in a numerical world with errors and alike. 

Numerical methods of solving optimization include brute force methods, that are not recommended for any complex methods, and iterative and stochastic methods. In iterative methods one tries to iteratively shrink down the solution space and/or arrive at the closest possible value for the objective function. Stochastic or random methods like Monte Carlo methods just pick values from a random distribution (of measured values) to fit to the objective function. Both iterative and stochastic methods are sometimes employed simultaneously to improve the overall result.  
A multitude of libraries exist with pre-written functions to apply on many problems, including ones for Python. The most standard and widely used is SciPy with several solver algorithms. Other packages are lmfit for leastsquare optimization or cvxpy for convex optimization. Cvxpy was also introduced in the context of portfolio optimization, a economical science-related technique of trying to maximize the profit while balancing out risk and investment.  
Another lesson dealt with the computer science topic of algorithmic optimization. With the examples of matrix multiplication and wave equations, several approaches were compared that each tried to make the code faster. These approaches included writing the code yourself, as package functions are restricted in terms of computing speed to accommodate for flexibility. Further, vectorization as a pythonic principle was mentioned that should be favored over looping/iterations. Lastly, compiling Python code into C with Numba is also a powerful approach, but has the disadvantage of not working well with code from libraries.  

Next the context of optimization is summarized within the field of microbial modeling. One of the most widely used methods in computational biology is so-called Flux Balance Analysis (FBA)<sup>[1], [2]</sup>. There, the metabolic network of a cell is simulated with just using the stoichiometric information of all reactions. With thermodynamic constraints, an optimization problem is formulated where the goal is the maximization of a given reaction flux – in most cases the biomass production = growth.   

max <a href="https://www.codecogs.com/eqnedit.php?latex=c&space;\cdot&space;v" target="_blank"><img src="https://latex.codecogs.com/png.latex?c&space;\cdot&space;v" title="c \cdot v" /></a>  
s.t. <a href="https://www.codecogs.com/eqnedit.php?latex=S&space;\cdot&space;v&space;=&space;0" target="_blank"><img src="https://latex.codecogs.com/png.latex?S&space;\cdot&space;v&space;=&space;0" title="S \cdot v = 0" /></a>  
<a href="https://www.codecogs.com/eqnedit.php?latex=\:\:\:\:\:\:&space;v_{i,&space;min}&space;\leq&space;v_i&space;\leq&space;v_{i,&space;max}" target="_blank"><img src="https://latex.codecogs.com/png.latex?\:\:\:\:\:\:&space;v_{i,&space;min}&space;\leq&space;v_i&space;\leq&space;v_{i,&space;max}" title="\:\:\:\:\:\: v_{i, min} \leq v_i \leq v_{i, max}" /></a>  

<a href="https://www.codecogs.com/eqnedit.php?latex=S" target="_blank"><img src="https://latex.codecogs.com/png.latex?S" title="S" /></a> is the stoichiometric matrix, <a href="https://www.codecogs.com/eqnedit.php?latex=v" target="_blank"><img src="https://latex.codecogs.com/png.latex?v" title="v" /></a> is the vector containing all reaction fluxes, and <a href="https://www.codecogs.com/eqnedit.php?latex=v_{i,min}" target="_blank"><img src="https://latex.codecogs.com/png.latex?v_{i,min}" title="v_{i,min}" /></a> and <a href="https://www.codecogs.com/eqnedit.php?latex=v_{i,max}" target="_blank"><img src="https://latex.codecogs.com/png.latex?v_{i,max}" title="v_{i,max}" /></a> represent lower and upper bounds respectively for each reaction flux. The first constraint equation is a derivation from the assumption that the stoichiometric coefficients multiplied with the reaction fluxes equal the rate-of-change of the metabolite concentrations. However, the rate-of-change can assumed to stay constant, and thus it is zero.

The result is then a distribution of all the flux values of all reactions, which can then be used to derive insights for foundational or applied science. For example, in the field of metabolic engineering the goal is to engineer microbes for the production of desired compounds – either natural or from other organisms. For this one needs to modify the metabolic network, like knocking out genes (preventing the expression of the phenotype of that gene) for detrimental reactions or overexpressing other genes for reactions that are important, or maybe even a bottleneck for the overproduction of the final compound. FBA can help in assessing the reaction network of interest. Modifications of FBA like MOMA <sup>[3]</sup> or ROOM <sup>[4]</sup> also specifically deal with organisms containing gene-knockouts  and can help to assess aspects like growth rate after knockouts, as suppressing a gene expression can lead to some unplanned effects. The problem with FBA is that it represents more of a qualitative method, than quantitative, as there are still large discrepancies between simulation and experimental measurements <sup>[5]</sup>. Still, FBA is powerful in terms of what it delivers compared to what it requires, so it still does represent a widely used method <sup>[6]</sup> that is also expanded with several modifications <sup>[2]</sup>.  
One of these methods is dynamic FBA, where the results of FBA are used as parameter values for rate-of-change descriptions of biological entities, like molecules, enzymes or whole cells <sup>[7]</sup>. While simple on the first glance, actually integrating the system of ODE’s just like that results in some problems with the integrator algorithm <sup>[8]</sup>. In short, the integration may fail because the FBA problem yields infeasible results that are transformed into null values <sup>[8]</sup>.  Gomez et al. solved this issue by including a second optimization problem into the FBA formulation <sup>[8]</sup>: the constraint of steady state metabolite changes <a href="https://www.codecogs.com/eqnedit.php?latex=S&space;\cdot&space;v&space;=&space;0" target="_blank"><img src="https://latex.codecogs.com/png.latex?S&space;\cdot&space;v&space;=&space;0" title="S \cdot v = 0" /></a> is relaxed by including two relaxation vectors <a href="https://www.codecogs.com/eqnedit.php?latex=s_&plus;" target="_blank"><img src="https://latex.codecogs.com/png.latex?s_&plus;" title="s_+" /></a> and <a href="https://www.codecogs.com/eqnedit.php?latex=s_-" target="_blank"><img src="https://latex.codecogs.com/png.latex?s_-" title="s_-" /></a>. These parameters are found via minimization with the constraints such that <a href="https://www.codecogs.com/eqnedit.php?latex=S&space;\cdot&space;v&space;=&space;s_&plus;&space;-&space;s_-" target="_blank"><img src="https://latex.codecogs.com/png.latex?S&space;\cdot&space;v&space;=&space;s_&plus;&space;-&space;s_-" title="S \cdot v = s_+ - s_-" /></a> is fulfilled. Optimally these values should add to zero, but in some cases a very small value is acceptable as well. Then these new vales are used in the modified FBA problem.  

min <a href="https://www.codecogs.com/eqnedit.php?latex=\sum\limits_{i=1}^n&space;s_&plus;&space;-&space;s_-" target="_blank"><img src="https://latex.codecogs.com/png.latex?\sum\limits_{i=1}^n&space;s_&plus;&space;-&space;s_-" title="\sum\limits_{i=1}^n s_+ - s_-" /></a>  
s.t. <a href="https://www.codecogs.com/eqnedit.php?latex=S&space;\cdot&space;v&space;&plus;&space;s_&plus;&space;-&space;s_-&space;=&space;0" target="_blank"><img src="https://latex.codecogs.com/png.latex?S&space;\cdot&space;v&space;&plus;&space;s_&plus;&space;-&space;s_-&space;=&space;0" title="S \cdot v + s_+ - s_- = 0" /></a>  
<a href="https://www.codecogs.com/eqnedit.php?latex=\:\:\:\:\:\:&space;v_{i,&space;min}&space;\leq&space;v_i&space;\leq&space;v_{i,&space;max}" target="_blank"><img src="https://latex.codecogs.com/png.latex?\:\:\:\:\:\:&space;v_{i,&space;min}&space;\leq&space;v_i&space;\leq&space;v_{i,&space;max}" title="\:\:\:\:\:\: v_{i, min} \leq v_i \leq v_{i, max}" /></a>  
<a href="https://www.codecogs.com/eqnedit.php?latex=\:\:\:\:\:\:&space;s_&plus;&space;\geq&space;0,&space;s_-&space;\geq&space;0" target="_blank"><img src="https://latex.codecogs.com/png.latex?\:\:\:\:\:\:&space;s_&plus;&space;\geq&space;0,&space;s_-&space;\geq&space;0" title="\:\:\:\:\:\: s_+ \geq 0, s_- \geq 0" /></a>  

max <a href="https://www.codecogs.com/eqnedit.php?latex=c&space;\cdot&space;v" target="_blank"><img src="https://latex.codecogs.com/png.latex?c&space;\cdot&space;v" title="c \cdot v" /></a>  
s.t. <a href="https://www.codecogs.com/eqnedit.php?latex=S&space;\cdot&space;v&space;&plus;&space;s_&plus;&space;-&space;s_-&space;=&space;0" target="_blank"><img src="https://latex.codecogs.com/png.latex?S&space;\cdot&space;v&space;&plus;&space;s_&plus;&space;-&space;s_-&space;=&space;0" title="S \cdot v + s_+ - s_- = 0" /></a>  
<a href="https://www.codecogs.com/eqnedit.php?latex=\:\:\:\:\:\:&space;v_{i,&space;min}&space;\leq&space;v_i&space;\leq&space;v_{i,&space;max}" target="_blank"><img src="https://latex.codecogs.com/png.latex?\:\:\:\:\:\:&space;v_{i,&space;min}&space;\leq&space;v_i&space;\leq&space;v_{i,&space;max}" title="\:\:\:\:\:\: v_{i, min} \leq v_i \leq v_{i, max}" /></a>  

This then shows how optimization is very much used abundantly in computational biology to simulate microbial metabolism and growth.


---
# References

Include references to papers you found that help illustrate your argument. I prefer references with links to papers.  


**Side hack:** Also, I love [zotero](https://www.zotero.org/) and I really like Jupyter notebooks. I would love it if one of you got board and figured out how to cite references and build a bibliography inside notebooks.  Something like [cite2c](https://github.com/takluyver/cite2c) looks like it would be really cool but I haven't tried it.

[1] Feist, A. M., & Palsson, B. Ø. (2008). The growing scope of applications of genome-scale metabolic reconstructions using Escherichia coli. Nature biotechnology, 26(6), 659-667.  
[2] Lewis, N. E., Nagarajan, H., & Palsson, B. O. (2012). Constraining the metabolic genotype–phenotype relationship using a phylogeny of in silico methods. Nature Reviews Microbiology, 10(4), 291-305.  
[3] Mahadevan, R., & Schilling, C. H. (2003). The effects of alternate optimal solutions in constraint-based
genome-scale metabolic models. Metabolic engineering, 5(4), 264-276.  
[4] Shlomi, T., Berkman, O., & Ruppin, E. (2005). Regulatory on/off minimization of metabolic flux changes
after genetic perturbations. Proceedings of the national academy of sciences, 102(21), 7695-7700.  
[5] Zhang, C., & Hua, Q. (2016). Applications of genome-scale metabolic models in biotechnology and
systems medicine. Frontiers in physiology, 6, 413.  
[6] Kim, W. J., Kim, H. U., & Lee, S. Y. (2017). Current state and applications of microbial genome-scale
metabolic models. Current Opinion in Systems Biology, 2, 10-18.  
[7] Mahadevan, R., Edwards, J. S., & Doyle III, F. J. (2002). Dynamic flux balance analysis of diauxic growth
in Escherichia coli. Biophysical journal, 83(3), 1331-1340.  
[8] Gomez, J. A., Höffner, K., & Barton, P. I. (2014). DFBAlab: a fast and reliable MATLAB code for dynamic flux balance analysis. BMC bioinformatics, 15(1), 1-10.  

-----
### Congratulations, you are done!

Now, you just need to submit this report by uploading it to the course <a href="https://d2l.msu.edu/">Desire2Learn</a> web page for today's dropbox.


