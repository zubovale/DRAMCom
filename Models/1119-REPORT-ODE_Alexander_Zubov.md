In order to successfully complete this assignment you need to turn in a project proposal to D2L on or before **11:59pm on Friday November 19**.

# <center>Using Ordinary Differential Equations (ODEs) in Microbial Modeling</center>

<center>by Alexander Zubov</center>




---
# Instructions


Write a ~1 (or 2) page report about how ODE (or PDE) models could be used in **your area of research**. If you do not have an area of research set, write about how ODE (or PDE) could be used in **your project**.

- Summarize what you learned about differential equations.

There are three basic forms to this report:

1. ODE (or PDE) is already used in your area of research.  Provide a summary and include references.  If possible include a short example that could be demonstrated in class.

2. Describe how you could incorporate ODE (or PDE) into your research domain.  What research questions might you be able to ask, improve or validate?

3. ODE (or PDE) cannot be used in your area of research.  Explain in detail why they can't be used.  

To be clear, I think 1 and 2 will be the most common reports.  If you choose 3 you need to make a very convincing argument.  Use your imaginations!  The best idea come from combining expertise in seemingly unrelated topics. 

---

A very abundant and important topic for computational sciences is the topic of Differential Equations. Differential Equations describe the rate of change of certain phenomena or mechanisms or alike, and are highly useful for modeling them.  
The reasoning behind even using them is that for a lot of both natural as well as artificial phenomena, differential equations provide both an intuitive and the only way to describe them - i.e., one does not know the actual, absolute change of how for example temperature changes over time, but only the relative change with respect to certain parameters.  
In contrast to algebraic equations which have function values as their solutions, differential equations have a function itself as a solution, though this is more so for analytical procedures - in computational sciences with numerical calculations, the solutions are function values as well. However, the process of solving differential equations, or a system of them respectively, is still different than the solution to an algebraic equation system. The reason is that differential equations with respect to certain variables, are in essence the derivative of certain functions of these same variables. Hence, to calculate the values of the functions, one needs to get from the derivative to the function, which is done via integration. In other words, the solutions to a system of differential equations are values of the antiderivatives of the rate of change functions. The most common way of numerical integration is solving an initial value problem, i.e. one has to provide a set of initial variable values with which the integrator algorithm is able to start the integration. Many different methods exist of solving such problems, the most simple one is forward Euler integration, but there exist more sophisticated ones like Runge-Kutta-type methods.

The most commonly used type, as well as the easiest one to solve, are ordinary differential equations. They describe the total rate of change of a variable with respect to only this dependent variable. There are more complex types of differential equations though, like partial differential equations that describe multivariant-dependent functions and are more difficult to solve. Even more complex are systems containing both differential equations and algebraic ones, called differential algebraic equations - they are useful for example to maintain certain bounds on the solution space for the dependent variables.  
The standard way of solving ODE's in Python specifically - and one of the few ones at that - is using the SciPy package, specifically the functions `odeint()` or alike and `solve_ivp()`. There exist other packages targeting PDE's or other types specifically, but all of them - including SciPy - are rather inefficient and slow for solving. Old FORTRAN libraries or the new language julia provide far better methods of solving a wider array of differential equations. Still, for the most common type, ODE's, SciPy in Python is more than sufficient.  

As ODE's are a very useful way to describe natural phenomena, they are abundantly used in biological research. One of the oldest mathematical descriptions in biology is the so-called Monod equation describing the growth of an organism on a substrate with the form of a rectangular hyperbola <sup>[1]</sup>  
<center>$ \mu = \mu_max \frac{S}{K_S + S} $</center>  
where $\mu$ is the specific growth rate dependent on the maximum possible growth rate $\mu_max$, the substrate concentration $S$ and the half saturation constant $K_S$. The actual growth of an organism $X$ can then be described with by multiplying this specific rate with the concentration of the organism itself to obtain the bulk rate, as well as multiplying it with a factor $y$ denoting the efficiency of the conversion of substrate $S$ to organismic biomass $X$. A simple system of ODE's looks then as follows:
<center>$ \frac{dX}{dt} = y \mu_max \frac{S}{K_S + S} S $</center>  
<center>$ \frac{dS}{dt} = - \mu_max \frac{S}{K_S + S} S $.</center>  
Even though the basic mechanics are simple, heavily parameterizing it with a multitude of additional terms resulted in massive models used for researching oceanic ecosystems <sup>[2], [3]</sup>.  
There are also slightly other ways of including ODE's in microbial modeling. The types of models introduced previously are a type of so-called "black box models" that treat microorganisms as a totality <sup>[4]</sup>. However there exist a method of including the inside network of a cell into the simulation. FBA is a constraint-based method that only requires the stoichiometric information about the metabolic network of a cell and formulates that into a linear programming problem (LP) that optimizes for growth and further outputs a distribution of all the reactions fluxes <sup>[5]</sup>. In other words, the computational output of FBA is a steady state situation, but rates of change nevertheless. These can then be used as rate of change terms in an ODE system, which is called dynamic FBA <sup>[6]</sup>. dFBA is much more computationally intensive than a much simple kinetic model using only ODE's, as the FBA LP has to be solved for every iteration, and on top of that there are other computational issues that can arise <sup>[7]</sup>. Still, dFBA is a useful method that combines advantages of both kinetic and constraint-based metabolic simulation techniques and at last shows how practical ODE's are in life sciences.

---
# References

[1] Monod, J. (1949). The growth of bacterial cultures. Annual review of microbiology, 3(1), 371-394.  
[2] Aumont, O., Maier‐Reimer, E., Blain, S., & Monfray, P. (2003). An ecosystem model of the global ocean including Fe, Si, P colimitations. Global Biogeochemical Cycles, 17(2).  
[3] Aumont, O., Éthé, C., Tagliabue, A., Bopp, L., & Gehlen, M. (2015). PISCES-v2: an ocean biogeochemical model for carbon and ecosystem studies. Geoscientific Model Development, 8(8), 2465-2513.  
[4] Saadat, N. P., Nies, T., Rousset, Y., & Ebenhöh, O. (2020). Thermodynamic limits and optimality of microbial growth. Entropy, 22(3), 277.  
[5] Feist, A. M., & Palsson, B. Ø. (2008). The growing scope of applications of genome-scale metabolic reconstructions using Escherichia coli. Nature biotechnology, 26(6), 659-667.  
[6] Mahadevan, R., Edwards, J. S., & Doyle III, F. J. (2002). Dynamic flux balance analysis of diauxic growth
in Escherichia coli. Biophysical journal, 83(3), 1331-1340.
[7] Gomez, J. A., Höffner, K., & Barton, P. I. (2014). DFBAlab: a fast and reliable MATLAB code for dynamic flux balance analysis. BMC bioinformatics, 15(1), 1-10.  

-----

### Congratulations, you are done!

Now, you just need to commit and push this report to your project git repository. 
