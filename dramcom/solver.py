"""The main code of the package, containing the Model and its underlying functions for solving.

The Model() object designating the dRBA model one aims to construct. Contains
methods for constructing and modifying the model, solving it and returning the results of
the simulation as well as saving them for storage.
"""
from rba import Results
from scipy.integrate import solve_ivp
import numpy as np
from IPython.display import display, Latex
import pandas as pd
from RBA_ConstraintMatrix_mod import ConstraintMatrix_mod
from RBA_Solver_mod import Solver_mod
from results import sol_Results

class Model():
    """The dRBA model.

    Consists of several parts making up the whole dynamic model:

    SBML-type model -- the dynamic model needs a metabolic model as a foundation to
    derive the necessary parameters from that network
    The dynamic model requires at least one SBML-type model. For more models, a
    dictionary needs to be put in containing the names of the models and the models themselves

    Initial constraints -- the constraints that will determine the output of the
    linear programming subproblem from the metabolic model

    ODE's -- the main part of the model, the ordinary differential equations
    describing the rate of change of the system variables

    Results -- the results of the simulation, mainly the values of the system
    variables, metabolic flux distributions and the protein content of the cell
    """

    def __init__(self, rba_models):
        """Initialize the dRBA model based on an underlying metabolic model.

        The core variables of the model are:

        rba_models -- either a model or a dictionary containing model names and their
        corresponding model

        constraints -- the external metabolites that constrain the RBA LP

        odes -- the system of ordinary differential equations describing the rate of change
        of the system variables

        sol_res -- the simulation result, the antiderivative values of the ODE system
        """
        self.rba_models = {}

        if type(rba_models) == dict:
            for name, model in rba_models.items():
                self.rba_models[name] = model
        else:
            self.rba_models["Model"] = rba_models

        self.constraints = {}
        self.odes = {}
        self.sol_res = None

    def add_constr(self, constraints):
        """Add the initial constraints to the model.

        Add constraints that determine the output of the subsequent linear optimization
        that creates the steady-state flux distribution as the output per time point.
        More constraints can be added to an existing list.

        NOTE: Make sure that the order of the constraints and the ODES correspond to each
        other!

        arguments:

        constraints -- needs to be either a list containing the string names for the underlying
        RBA LP, or a dictionary with models as keys and a list of constraints as the value.
        NOTE: Make sure to put the correct metabolite ID's as constraint names! Double check
        with the ID's of the RBA metabolic model!

        examples:

        test_model.add_constr([M_glc__D])
        test_model.constraints
        >>> {"Model": [M_glc__D]}

        test_model.add_constr({"model1": [M_glc__D], "model2": [M_co2, M_glc__D]})
        test_mmodel.constraints
        >>> {"model1": [M_glc__D], "model2": [M_co2, M_glc__D]}
        """
        if type(constraints) == dict:
            if bool(self.constraints) is not False:
                for model, constr in constraints.items():
                    original = self.constraints.get(model)
                    new_constraints = original + constr
                    self.constraints[model] = new_constraints
            else:
                for model, constr in constraints.items():
                    self.constraints[model] = constr
        else:
            if bool(self.constraints) is not False:
                original = self.constraints.get("Model")
                new_constraints = original + constraints
                self.constraints["Model"] = new_constraints
            else:
                self.constraints["Model"] = constraints

    def add_ode(self, odes):
        """Add the ordinary differential equations (ODE's) to the model.

        Add information about the rate of change of the system variables in the
        dynamic RBA model.

        NOTE: Make sure that the order of the constraints and the ODES correspond to each
        other!

        arguments:

        odes -- either a dictionary or a list. In the simpler case of a dict, the system variables
        (left hand side) are the keys and the valeus are the right hand side informationsof the
        differential equations. In the case of a list, when more than one metabolic model is
        described, the argument is one list containing tuples for each metabolic model - the first
        argument of the tuple is the model name, the second a dictionary as described before
        containing the right hand side information.
        The right hand side information itself is then a list of doublet touples as well, where the
        first argument is a string of one of three mathematical operators - "+", "-" and "*" - and
        the second the numerical value that is used to modify the right hand side.
        The second tuple argument can also be a tuple itself with the first argument containing a
        model name and the second a numerical value.

        Repeated calls to this function with changes to the right hand side information update the
        corresponding values.

        examples:

        test_model.add_ode({"biomass": [("*", 1)], "M_glc__D": [("*", 0.01)]})
        test_model.odes
        >>> {'Model': {'biomass': [('*', 1)], 'R_EX_glc__D_e': [('*', 0.01)]}}

        test_model2.add_ode(
            [("E. coli wt",
                {"biomass": [("*", 1)],
                 "R_EX_glc__D_e": [("*", 0.02)]}),
             ("E. coli CO2",
                 {"biomass": [("*", 1)],
                  "R_EX_co2_e": [("*", 0.01)]})]
        )
        >>> {'E. coli wt': {'biomass': [('*', 1)], 'R_EX_glc__D_e': [('*', 0.02)]},
             'E. coli CO2': {'biomass': [('*', 1)], 'R_EX_co2_e': [('*', 0.01)]}}

        test_model2.add_ode(
            [("E. coli wt", {"biomass": [("*", 1), ("-", ("E. coli wt", 0.01))],
                             "R_EX_glc__D_e": [("*", 0.012), ("+", ("E. coli CO2", 4))]}),
             ("E. coli CO2", {"biomass": [("*", 1), ("-", ("E. coli CO2", 0.01))],
                              "R_EX_co2_e": [("*", 0.01), ("+", ("E. coli wt", 6))]})]
        )
        >>> {'E. coli wt': {'biomass': [('*', 1), ('-', ('E. coli wt', 0.01))],
                            'R_EX_glc__D_e': [('*', 0.012), ('+', ('E. coli CO2', 4))]},
             'E. coli CO2': {'biomass': [('*', 1), ('-', ('E. coli CO2', 0.01))],
                             'R_EX_co2_e': [('*', 0.01), ('+', ('E. coli wt', 6))]}}
        """
        if type(odes) == list:
            if bool(self.odes) is not False:
                for model, roch in self.odes.items():
                    original = roch
                    for new_model, new_roch in odes:
                        if new_model == model:
                            for roch_var, roch_term in new_roch.items():
                                original[roch_var] = roch_term
                    self.odes[model] = original
            else:
                for model, roci in odes:
                    self.odes[model] = roci
        else:
            if bool(self.odes) is not False:
                original = self.odes.get("Model")
                for roch_var, roch_term in odes.items():
                    original[roch_var] = roch_term
                self.odes["Model"] = original
            else:
                self.odes["Model"] = dict(odes.items())

    def drba(self, t, y, no_neg = True):
        """Update all values and solve for one iteration.

        Uses the information from external constraints to compute the RBA base LP with adjusted
        feasibility, then modify the steady state output with the right hand side information
        provided to construct the rates of change of the system.
        Returns a numpy.array of the rates of change.
        Outside calls to this function are normally only necessary for debugging or exploring of
        the code.

        arguments:

        t -- the time point, required for the solve_ivp function

        y -- the initial values and then late antiderivative values of the system variables

        no_neg -- determines if negative variable values should result in a zero rate of change,
        as to prevent any negative values from forming.
        """
        assign_l = []
        roct_no_tot = 0
        for model, roci in self.odes.items():
            roct_no = len(roci)
            assign_l.append((model, roct_no))
            roct_no_tot += roct_no

        y_split_l = []
        count = 0
        for assigning in assign_l:
            y_split = y[0 + count : assigning[1] + count]
            y_split_l.append(y_split)
            count += assigning[1]

        for y_part, constr_i in zip(y_split_l, self.constraints.items()):
            biomass = y_part[0]
            model_name = constr_i[0]
            constraints = constr_i[1]

            for constraint, val in zip(constraints, y_part[1:]):
                if constraint == "M_glc__D":
                    if val <= 0:
                        val = 0.0005
                self.rba_models.get(model_name).medium[constraint] = val


        model_lps = {}
        for name, model in self.rba_models.items():
            model_mat = ConstraintMatrix_mod(model)
            lp_solver = Solver_mod(model_mat)
            lp_solver.solve()
            lp_results = Results(model, model_mat, lp_solver)
            model_lps[name] = lp_results


        dy_l = []
        for ss_results, rochs in zip(model_lps.items(), self.odes.items()):
            ss_result = ss_results[1]
            roch = rochs[1]

            for roch_var, roch_term in roch.items():
                if roch_var == "biomass":
                    dvar = ss_result.mu_opt * biomass
                    for operator, value in roch_term:
                        if type(value) != tuple:
                            if operator == "+":
                                dvar += value
                            elif operator == "-":
                                dvar -= value
                            elif operator == "*":
                                dvar *= value
                    dy_l.append(dvar)
                else:
                    dvar = ss_result.reaction_fluxes().get(roch_var) * biomass
                    for operator, value in roch_term:
                        if type(value) != tuple:
                            if operator == "+":
                                dvar += value
                            elif operator == "-":
                                dvar -= value
                            elif operator == "*":
                                dvar *= value

                    if ss_result.mu_opt <= 0.05:
                        dvar *= 0
                    dy_l.append(dvar)

        biomass_ind_dict = {}
        count = 0
        for (name, model), y_split in zip(self.rba_models.items(), y_split_l):
            roct_no_tot = len(y_split)
            count += roct_no_tot
            biomass_ind = count - roct_no_tot
            biomass_ind_dict[name] = biomass_ind

        ind_dict = {}
        count = 0
        for model, roch in self.odes.items():
            for roch_var, roch_term in roch.items():
                ind_dict[roch_var] = count
                count += 1
        ind_dict = {rxn: ind for rxn, ind in ind_dict.items() if rxn not in biomass_ind_dict}

        for model, roch in self.odes.items():
            for roch_var, roch_term in roch.items():
                if roch_var == "biomass":
                    for operator, value in roch_term:
                        if type(value) == tuple:
                            assign = value[0]
                            lp_result = model_lps[assign]
                            val = value[1]
                            if operator == "+":
                                dy_l[biomass_ind_dict.get(assign)] += lp_result.mu_opt * val
                            elif operator == "-":
                                dy_l[biomass_ind_dict.get(assign)] -= lp_result.mu_opt * val
                            elif operator == "*":
                                dy_l[biomass_ind_dict.get(assign)] *= lp_result.mu_opt * val
                else:
                    for operator, value in roch_term:
                        if type(value) == tuple:
                            assign = value[0]
                            lp_result = model_lps[assign]
                            val = value[1]
                            if operator == "+":
                                dy_l[ind_dict.get(roch_var)] += lp_result.mu_opt * val
                            elif operator == "-":
                                dy_l[ind_dict.get(roch_var)] -= lp_result.mu_opt * val
                            elif operator == "*":
                                dy_l[ind_dict.get(roch_var)] *= lp_result.mu_opt * val

        if no_neg == False:
            count = 0
            for val in y:
                if val <= 0:
                    ind = y.index(val, count)
                    dy_l[ind] *= 0
                count += 1

        dy = np.array(dy_l)
        return dy

    def solve(self, initials, t, method = "LSODA"):
        """Solve the dRBA model.

        Solve the constructed model based on an initial value problem. Requires the
        initial values as well as the time span.

        arguments:

        initials -- the initial values of the system variables

        t -- the time span for the integration

        method -- the integration method used by scipy's solve_ivp suite. The default
        method is the powerful LSODA algorithm (flexible switching between stiff and
        non-stiff methods for solving differential equations)
        """

        ts = t
        y0 = initials

        self.sol_res = solve_ivp(
            fun = self.drba,
            t_span = (ts.min(), ts.max()),
            y0 = y0,
            t_eval = ts,
            rtol = 1e-1,
            atol = 1e-2,
            method = method
        )

        self.reset_medium(initials)


    def results(self, df = None):
        """Return the computed results of the simulation.

        The antiderivative values are used to re-compute the values of other relevant
        variables in the RBA LP for reach timepoint. The function then returns an object
        storing the LP results that can be further used for more intricate investigations
        of the simulation.

        arguments:

        df -- alternatively to the the directly computed solution of the solve method, a
        dataframe can also be passed to construct the results object.
        """
        if type(df) == pd.DataFrame:
            results = sol_Results(self, df)
        else:
            results = sol_Results(self, self.sol_res.y.T)

        results.comp_lp_res()

        return results

    def get_model(self):
        """Get the structure of the model.

        Returns a string representation of the model structure that lists the metabolic
        models used, the external constraints, and the system of ODEs. The ODE system
        is returned in a LaTeX representation.

        examples:

        test_model.get_model()
        >>> RBA models used:
        --------------------------
        -Model

        Constraints:
        --------------------------
        Model:
        -M_glc__D


        ODEs:
        --------------------------
        Model:

        𝑑𝑀𝑜𝑑𝑒𝑙𝑑𝑡=1𝜇𝑀𝑜𝑑𝑒𝑙
        𝑑𝑔𝑙𝑐−−𝐷−𝑒𝑑𝑡=1𝜌𝑔𝑙𝑐−−𝐷−𝑒𝑀𝑜𝑑𝑒𝑙
        """
        print("RBA models used:")
        print("--------------------------")
        for name, model in self.rba_models.items():
            print("-"+name, end = "  ")
        print()
        print()

        print("Constraints:")
        print("--------------------------")
        for model, constraints in self.constraints.items():
            print(model+":")
            for constraint in constraints:
                print("-"+constraint)
            print()
        print()

        print("ODEs:")
        print("--------------------------")

        for model, rochs in self.odes.items():
            print(model+":")
            for roch_var, roch_term in rochs.items():
                if roch_var == "biomass":
                    out = f"$ \\frac{{d{model}}}{{dt}} =$"
                    for operator, value in roch_term:
                        if type(value) != tuple:
                            if operator == "+":
                                out += f" $+ {value}$ "
                            elif operator == "-":
                                out += f" $- {value}$ "
                            elif operator == "*":
                                out += f" ${value}\\mu {model}$ "
                        else:
                            assign = value[0]
                            new_val = value[1]
                            if operator == "+":
                                out += f" $+ {new_val}{assign}$ "
                            elif operator == "-":
                                out += f" $- {new_val}{assign}$ "
                            elif operator == "*":
                                out += f" $* {new_val}{assign}$ "
                    display(Latex(out))
                else:
                    if "R_EX_" in roch_var:
                        roch_var = roch_var.replace("R_EX_", "")
                    roch_var = roch_var.replace("_", "-")
                    out = f"$ \\frac{{d{roch_var}}}{{dt}} =$"
                    for operator, value in roch_term:
                        if type(value) != tuple:
                            if operator == "+":
                                out += f" $+ {value}$ "
                            elif operator == "-":
                                out += f" $- {value}$ "
                            elif operator == "*":
                                out += f" ${value} \\rho_{{{roch_var}}} {model}$ "
                        else:
                            assign = value[0]
                            new_val = value[1]
                            if operator == "+":
                                out += f" $+ {new_val}{assign} $"
                            elif operator == "-":
                                out += f" $- {new_val}{assign} $"
                            elif operator == "*":
                                out += f" $* {new_val}{assign} $"
                    display(Latex(out))

    def reset_medium(self, initials):
        """Resets the medium to the initial concentrations defined by the model.

        A method used at the end of the call to solve() to reset the medium concentrations,
        as the drba() method redefines these at every integration time point. Resetting these
        values to the initial values the possessed allows repeating any simulations.

        arguments:

        initials -- the initial values that are used to reset the external metabolite concentrations
        """
        y = initials

        assign_l = []
        roct_no_tot = 0
        for model, roci in self.odes.items():
            roct_no = len(roci)
            assign_l.append((model, roct_no))
            roct_no_tot += roct_no

        y_split_l = []
        count = 0
        for assigning in assign_l:
            y_split = y[0 + count : assigning[1] + count]
            y_split_l.append(y_split)
            count += assigning[1]

        for y_part, constr_i in zip(y_split_l, self.constraints.items()):
            model_name = constr_i[0]
            constraints = constr_i[1]

            for constraint, val in zip(constraints, y[1:]):
                self.rba_models.get(model_name).medium[constraint] = val

    def remove_constraints(self, constraints):
        """Remove constraints in the model.

        Allows the removal of external metabolite constraints. Structurally similar to the
        add_costr() method.

        NOTE: Make sure that the order of the constraints and the ODES correspond to each
        other! Modifications of the self.odes variable may be necessary.

        arguments:

        constraints -- needs to be either a list containing the string names for the underlying
        RBA LP, or a dictionary with models as keys and a list of constraints as the value.
        NOTE: Make sure to put the correct metabolite ID's as constraint names! Double check with
        the ID's of the RBA metabolic model!

        examples:

        test_model.constraints
        >>> {"Model": ["M_glc__D", "M_co2"]}
        test_model.remove_constraints(["M_co2"])
        test_model.constraints
        >>> {"Model": ["M_glc__D"]}

        test_model.constraints
        >>> {"model1": ["M_glc__D", "M_etoh"], "model2": ["M_co2", "M_nh4"]}
        test_model.remove_constraints({"model1": ["M_glc__D"], "model2": ["M_nh4"]})
        test_model.constraints
        >>> {"model1": ["M_etoh"], "model2": ["M_co2"]}
        """
        for model in self.constraints.keys():
            new_constraints = []
            if type(constraints) == dict:
                for model_assign in constraints:
                    if model_assign == model:
                        intersect = list(set(self.constraints.get(model)) & set(constraints.get(model)))
                        for constraint in self.constraints.get(model):
                            if constraint not in intersect:
                                new_constraints.append(constraint)
                        self.constraints[model] = new_constraints

            else:
                intersect = list(set(self.constraints.get(model)) & set(constraints))
                for constraint in self.constraints.get(model):
                    if constraint not in intersect:
                        new_constraints.append(constraint)
                self.constraints[model] = new_constraints

    def remove_ode(self, odes):
        """Remove odes in the model.

        Allows the removal of a system variable alongside is rate of change information.

        NOTE: Make sure that the order of the constraints and the ODES correspond to each
        other! Modifications of the self.constraints variable may be necessary.

        arguments:

        odes -- a dictionary containing the model name and the name of the system variable
        to-be-removed.

        examples:

        test_models.odes
        >>> {"Model": {"biomass": ("*", 1), "M_glc__D": ("+", 0.005), ("+", 0.01)}}
        test_models.remove_ode({"Model": "M_glc__D"})
        test_model.odes
        >>> {"Model": {"biomass": ("*", 1)}}
        """
        for model, roch in self.odes.items():
            for del_model, del_ode in odes.items():
                if del_model == model:
                    roch.pop(del_ode)

    def write_results(self, path = "", filename = "dRBA_sim"):
        """Save simulation results in dataframe

        Allows the storage of the simulation result in a csv-type data format.
        The simulation results are first converted into a pandas dataframe and
        then saved as a .csv. This then allows the easy reading of the file as
        a pandas dataframe and easy manipulations also included in the results
        package.

        arguments:

        path -- a path to a (sub)directory

        filename -- a custom filename. If none provided, "dRBA_sim" is used instead
        """
        col_names = []
        for model, constraints in self.constraints.items():
            col_names.append(model)
            for constraint in constraints:
                col_names.append(constraint)

        sol_df = pd.DataFrame(
            self.sol_res.y.T,
            columns = col_names
        )
        sol_df["t"] = self.sol_res.t
        col_names.insert(0, "t")
        sol_df = sol_df[col_names]

        full_path = path + filename + ".csv"
        sol_df.to_csv(full_path)
