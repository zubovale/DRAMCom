"""The results module.

Contains the results object with methods for re-calculating other RBA variables that were
lost during the integration process. Then allows the extraction of some of these for
further in-detail studies of the simulation, like protein concentrations.
"""
from rba import Results
import numpy as np
import pandas as pd
from RBA_ConstraintMatrix_mod import ConstraintMatrix_mod
from RBA_Solver_mod import Solver_mod
#from solver import Model

class sol_Results(object):
    """The results of the dynamic simulation.

    Defines a results object that itself contains the re-calculated LP results during all time
    points of the integration. Then allows the extraction of specific variable values using the
    RBA base package.
    """

    def __init__(self, model, sol_res):
        """Initialize the results object.

        Requires antiderivative values of the variables obtained by integrating the
        ODE system of the dynamic model.

        arguments:

        model -- the dRBA model, as some of its variables are required for the methods

        sol_res -- the solution of the dynamic simulation, can either be a numpy.array
        as returned via direct conversion of the result of the call to solve_ivp(), or
        instead a pandas dataframe. The latter option is desirable if one does not want
        to rerun the simulation and instead use saved results in a csv format.
        """
        self.model = model

        if type(sol_res) == np.ndarray:
            self.sol_res = sol_res
        elif type(sol_res) == pd.DataFrame:
            self.sol_res = self.df_to_ar(sol_res)

        self.model_lps = {}

    def df_to_ar(self, df):
        """Converts a pandas dataframe to a numpy array.

        Required by the comp_lp_res() method for recalculation of the other RBA variables.

        arguments:

        df -- a pandas dataframe
        """
        new_ar = df.to_numpy()
        new_ar2 = np.delete(new_ar, obj = np.s_[:1], axis = 1)
        return new_ar2

    def comp_lp_res(self):
        """Re-compute the LP results.

        Re-calculates the LP results that were transiently computed during the integration
        to obtain the right hand side information. For algorithmic reasons, one cannot save this
        values when using the ODE solver, hence they need to be re-computed using the variable
        values obtained by the solver.
        """
        sol = self.sol_res

        assign_l = []
        var_no_tot = 0
        for model, var_i in self.model.odes.items():
            var_no = len(var_i)
            assign_l.append((model, var_no))
            var_no_tot += var_no

        sol_split_l = []
        count = 0
        for assigning in assign_l:
            sol_split = sol[:, 0 + count : assigning[1] + count]
            sol_split_l.append(sol_split)
            count += assigning[1]

        #self.model_lps = {}
        for sol_part, model, constr_i in zip(sol_split_l, self.model.rba_models.items(),
        self.model.constraints.items()):
            #biomass = sol_part[:, 0]
            model = model[1]
            model_name = constr_i[0]
            constraints = constr_i[1]

            result_l = []
            for var_vals in sol_part:
                var_vals = var_vals[1:]

                for constraint, val in zip(constraints, var_vals):
                    if val <= 0:
                        val = 0.0005
                    self.model.rba_models.get(model_name).medium[constraint] = val

                model_mat = ConstraintMatrix_mod(model)
                lp_solver = Solver_mod(model_mat)
                lp_solver.solve()
                lp_results = Results(model, model_mat, lp_solver)
                result_l.append(lp_results)

            self.model_lps[model_name] = result_l

    def get_growth(self):
        """Return the optimal growth rates during the simulation.

        Uses the mu_opt variable from the base RBApy results file to extract
        the optimal growth rates of the organisms during the simulation.
        """
        mu_dict = {}
        for model, results in self.model_lps.items():
            mu_l = []
            for result in results:
                mu = result.mu_opt
                mu_l.append(mu)
            mu_dict[model] = mu_l
        return mu_dict

    def get_reactions(self):
        """Returns the reaction fluxes during the simulation.

        Uses the reaction_fluxes() method of the base RBApy package to extract
        the flux values of all reacation per each model that were achieved during the
        simulation.
        """
        rxn_dict = {}
        for model, results in self.model_lps.items():
            rxn_l = []
            for result in results:
                rxns = result.reaction_fluxes()
                rxn_l.append(rxns)
            rxn_dict[model] = rxn_l
        return rxn_dict

    def get_enzymes(self):
        """Return the enzyme concentrations during the simulation.

        Uses the enzyme_concentrations() method of the base RBApy package to extract
        the optimal enzyme concentrations per each model that were achieved during the
        simulation.
        """
        enzyme_dict = {}
        for model, results in self.model_lps.items():
            enzyme_l = []
            for result in results:
                enzymes = result.enzyme_concentrations()
                enzyme_l.append(enzymes)
            enzyme_dict[model] = enzyme_l
        return enzyme_dict
