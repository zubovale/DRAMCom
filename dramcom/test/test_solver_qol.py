import solver
import pytest
import rba
import numpy as np

test_rba_model1 = rba.RbaModel.from_xml("Escherichia-coli-K12-WT")
test_rba_model2 = rba.RbaModel.from_xml("Escherichia-coli-CO2-fixing")

def test_model_generation():
    #test the successful model generation and correct model initiation
    test_model1 = solver.Model(test_rba_model1)
    assert test_model1 != None
    assert test_model1.rba_models != None
    assert test_model1.sol_res == None
    
    test_model2 = solver.Model(
        {"E. coli wt": test_rba_model1,
         "E. coli CO2": test_rba_model2}
    )
    assert test_model2 != None
    assert test_model2.rba_models != None
    assert test_model2.sol_res == None
    assert len(test_model2.rba_models) == 2
    for input_model, output_model in zip(["E. coli wt", "E. coli CO2"], test_model2.rba_models.keys()):
        assert input_model == output_model

test_model1 = solver.Model(test_rba_model1)
test_model2 = solver.Model(
    {"E. coli wt": test_rba_model1,
     "E. coli CO2": test_rba_model2}
)


def test_add_constraints():
    #test the constraint-adding function
    test_model1.add_constr(["M_glc__D"])
    assert test_model1.constraints == {"Model": ["M_glc__D"]}
    test_model1.add_constr(["M_etoh"])
    assert test_model1.constraints == {"Model": ["M_glc__D", "M_etoh"]}
    
    test_model2.add_constr({"E. coli wt": ["M_glc__D"], "E. coli CO2": ["M_co2"]})
    assert test_model2.constraints == {"E. coli wt": ["M_glc__D"], "E. coli CO2": ["M_co2"]}
    test_model2.add_constr({"E. coli wt": ["M_etoh"], "E. coli CO2": ["M_nh4"]})
    assert test_model2.constraints == {"E. coli wt": ["M_glc__D", "M_etoh"], "E. coli CO2": ["M_co2", "M_nh4"]}

def test_remove_constraints():
    #test the constraint-removing function
    test_model1.remove_constraints(["M_glc__D"])
    assert test_model1.constraints == {"Model": ["M_etoh"]}
    
    test_model2.remove_constraints({"E. coli CO2": ["M_co2"]})
    assert test_model2.constraints == {"E. coli wt": ["M_glc__D", "M_etoh"], "E. coli CO2": ["M_nh4"]}



def test_add_odes():
    #test the ode-adding function
    test_model1.add_ode({"biomass": [("*", 1)], "R_EX_glc__D_e": [("*", 0.01)]})
    assert test_model1.odes == {"Model": {"biomass": [("*", 1)], "R_EX_glc__D_e": [("*", 0.01)]}}
    test_model1.add_ode({"biomass": [("*", 1)], "R_EX_glc__D_e": [("*", 0.02)]})
    assert test_model1.odes == {"Model": {"biomass": [("*", 1)], "R_EX_glc__D_e": [("*", 0.02)]}}
    
    test_model2.add_ode(
        [("E. coli wt",
          {"biomass": [("*", 1)],
           "R_EX_glc__D_e": [("*", 0.02)]}),
         ("E. coli CO2",
          {"biomass": [("*", 1)],
           "R_EX_co2_e": [("*", 0.01)]})]
    )
    assert test_model2.odes == {'E. coli wt': {'biomass': [('*', 1)], 'R_EX_glc__D_e': [('*', 0.02)]}, 
                                'E. coli CO2': {'biomass': [('*', 1)], 'R_EX_co2_e': [('*', 0.01)]}}
    test_model2.add_ode(
        [("E. coli wt", {"biomass": [("*", 1), ("-", ("E. coli wt", 0.1))], 
                         "R_EX_glc__D_e": [("*", 0.012), ("+", ("E. coli CO2", 8))]}),
         ("E. coli CO2", {"biomass": [("*", 1), ("-", ("E. coli CO2", 0.1))],
                          "R_EX_co2_e": [("*", 0.01), ("+", ("E. coli wt", 12))]})]
    )
    assert test_model2.odes == {'E. coli wt': {'biomass': [('*', 1), ('-', ('E. coli wt', 0.1))],
                                               'R_EX_glc__D_e': [('*', 0.012), ('+', ('E. coli CO2', 8))]},
                                'E. coli CO2': {'biomass': [('*', 1), ('-', ('E. coli CO2', 0.1))],
                                                'R_EX_co2_e': [('*', 0.01), ('+', ('E. coli wt', 12))]}}

def test_remove_odes():
    #test the ode-removing function
    test_model1.add_ode({"biomass": [("*", 1)], "R_EX_glc__D_e": [("*", 0.01)], "R_EX_etoh_e": [("-", 0.006)]})
    test_model1.remove_ode({"Model": "R_EX_glc__D_e"})
    assert test_model1.odes == {"Model": {"biomass": [("*", 1)], "R_EX_etoh_e": [("-", 0.006)]}}
    
    test_model2.add_ode(
        [("E. coli wt", {"biomass": [("*", 1), ("-", ("E. coli wt", 0.1))], 
                         "R_EX_glc__D_e": [("*", 0.012), ("+", ("E. coli CO2", 8))],
                         "R_EX_etoh_e": [("*", 0.07)]}),
         ("E. coli CO2", {"biomass": [("*", 1), ("-", ("E. coli CO2", 0.1))],
                          "R_EX_co2_e": [("*", 0.01), ("+", ("E. coli wt", 12))],
                          "R_EX_nh4_e": [("*", -0.004)]})]
    )
    test_model2.remove_ode({"E. coli wt": "biomass", "E. coli CO2": "R_EX_co2_e"})
    assert test_model2.odes == {"E. coli wt": {"R_EX_glc__D_e": [("*", 0.012), ("+", ("E. coli CO2", 8))], 
                                               "R_EX_etoh_e": [("*", 0.07)]},
                                "E. coli CO2": {"biomass": [("*", 1), ("-", ("E. coli CO2", 0.1))],
                                                "R_EX_nh4_e": [("*", -0.004)]}}



def test_drba():
    #test the solving of the adjusted linear programming problen and assembly of the
    #right hand side information
    ss_result = test_model1.drba(np.linspace(0, 3, 10), [1, 4])
    assert type(ss_result) == np.ndarray
    assert len(ss_result) == 2



def test_reset_medium():
    #test the medium-resetting function
    test_model1.reset_medium([0.1, 10])
    assert test_model1.rba_models["Model"].medium["M_glc__D"] == 10