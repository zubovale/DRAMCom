import solver
import pytest
import rba
import numpy as np
import pandas as pd

test_rba_model = rba.RbaModel.from_xml("Escherichia-coli-K12-WT")
test_rba_sim = pd.read_csv("HPCC_drba_test_2.csv", index_col = 'Unnamed: 0')

test_model = solver.Model(test_rba_model)
test_model.add_constr(["M_glc__D"])
test_model.add_ode(
    {"biomass": [("*", 1)],
    "R_EX_glc__D_e": [("*", 0.01)]}
)

def test_solver():
    #test the solving capability of the package
    ts = np.linspace(0, 3, 10) 
    y0 = [0.1, 10]
    test_model.solve(y0, ts)
    sol = test_model.sol_res
    assert sol != None
    assert type(sol.t) == np.ndarray
    assert sol.t.shape == (10,)
    assert type(sol.y.T) == np.ndarray
    assert sol.y.T.shape == (10, 2)



def test_results():
    #test the re-calculation capability of the results file
    results1 = test_model.results()
    mu_opts = results1.get_growth()
    assert type(mu_opts) == dict
    reactions = results1.get_reactions()
    assert type(reactions) == dict
    enzymes = results1.get_enzymes()
    assert type(enzymes) == dict
    
    results2 = test_model.results(df = test_rba_sim)
    mu_opts = results2.get_growth()
    assert type(mu_opts) == dict
    reactions = results2.get_reactions()
    assert type(reactions) == dict
    enzymes = results2.get_enzymes()
    assert type(enzymes) == dict