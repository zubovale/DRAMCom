# DRAMCom - Dynamic Resource Allocation in Microbial Communities

## Abstract

DRAMCom is a package designed to allow flexible construction of dynamic RBA (dRBA) models and their simulation. dRBA is a method to use the steady state output of a metbalic-resource allocation model in a sytem of differential equations for dynamic time-series developments of microorganisms. The package allows the construction of community models with at least a co-culture being confirmed to work via testing. The simulation results can then further be analyzed to extract other information that is calculated by the RBA base method.  

The packge is an enhancement building on the package called RBApy developed by the Systems Biology Team of the MaiAGE lab at the french INRAE institute <sup>[1]</sup>. The package uses code developed by the team for an ajusted calculation, as well as pre-built models for the development and tutorial. For further information, please see the teams GitHub for [RBApy](https://github.com/SysBioInra/RBApy) and [SysBioInra](https://github.com/SysBioInra) in general.

## Installation

DRAMCom has the following requirements:

- *SciPy version <= 1.2.1*  
this is due to requirements by a previous version of RBApy

- *Python version < 3.8*  
(due to the SciPy version requirement)

- *RBApy*  
NOTE: the package has been updated during development. To use the package, clone a version earlier than November 2021. To install this package, execute the `setup.py` file in the repository

- *IBM ILOG CPLEX*  
This is a commercial linear solver suite, but a free academic license can be obtained. To obtain one, you have to register under [https://academic.ibm.com/a2mt/email-auth] and login to your IBM account. After that, you will be redirected to a page titled 'Data Science'. Scroll down until you find a small menu on the left side and click on 'Software'. You will see several new boxes appear, one of them titled 'ILOG CPLEX Optimization Studio'. Click on the small arrow and aferwards on 'Downloads'. After that, clock on 'HTTP' to download the default .exe file. You can click on the drop down menu of the solver suite to select different versions for your respective operating system. Lastly, click on 'Download now' to complete the download. After downloading and installing the basic suite, you need to install the Python API to finish the actual installation for Python. Go to the directory with the 'python' folder and execute the `setup.py` file targeting the package directory of your Python/Anaconda installation/environment.

The basic python package requirements can be installed using the `requirements.yml` file.

----
## Background

Life on earth follows the interactions of a myriad of players. These players can be categorized into similar groups, and our current understanding describes the foundation of earths food web the interactions between photoautotrophs and chemoheterotrophs <sup>[2]</sup>. The former group includes algae and plants, the latter most other lifeforms including us. Nutrient exchange is likely the key factor driving these interactions <sup>[3]</sup>. To study these interactions, an alternative to large scale biogeochemical models currently employed <sup>[4]</sup> is to focus on smaller key interactions and try to elucidate them in more detail. One of them is the so-called microbial loop, which while describing the exchange of nutrients between a multitude of microbial (mostly prokaryotic) groups <sup>[5], [6]</sup>, can be abstracted as a more simple interaction - in the simplest way just a co-culture between a phototroph and a heterotroph.  

This has been done previously to construct a simple proof of concept model based on the method of dynamic Flux Balance Analysis (dFBA) <sup>[7]</sup>. dFBA is based on the constraint-based method of FBA where the information about a metabolic network is used to construct a linear optimization problem (or linear programming problem, LP) that calculates a steady state output with optimized growth rate and other reaction fluxes <sup>[8]</sup>. Making this dynamic involves constructing a system of ordinary differential equations (ODEs) that use the steady state iformation to construct the rate of change terms of the right hand side of this ODE system.  
A recent enhancement of FBA is called Resource Balance Analysis (RBA) where not only metabolites and their reactions are considered, but also the molecular machines carrying out these reactions - enzymes - as well as other macromolecules (DNA, other proteins, etc.) <sup>[9]</sup>. The assumption of RBA is that microorganisms are not only trying to grow optimally, but that they are further optimal resource allocators trying to balance out the usage of resources (energy, 'building-block' metabolites) between growth and other reactions on the one side, and the molecular machinery needed to carry them out on the other side. RBA can be made dynamic the same way as used for dFBA and would further allow the incorporation of other aspects into a dynamic community simulation.

## Objectives

The main goal of the project was to create a package that would allow the construction of a dynamic RBA model, and to simulate it. Sub-goals of this as well as optional ones are listed below. Due to time constraints, not all of them could be completed, but the basic package was and is functional with some basic quality of life methods included.

1. **Exploring RBApy and understanding its capabilites**  
The total scope of the RBApy package had to be researched as well as the functionality. This would then allow the usage and modifications to it in the later steps.

2. **Replicate results of previous computations with RBApy**  
The main goal that was only partly achieved was to replicate the results gathered by the Bachelors Thesis proof of concept model to simulate a stable co-culture between an autotrophic and heterotrophic organism (phototrophy was not included and due to the limited numver of pre-build RBA models also not possible). For this, an adjusted feasibility was incorporated by modifying two files of the RBApy package to construct the LP similarly as in DFBALab, a MATLAB package dealing with dFBA <sup>[10]</sup>.  

3. **Expand upon framework**  
The next logical step would have been to expand the package and allow more intricate analyses and descriptions that distinguish RBA from FBA. This could not be achieved however.

**Optional**

4. **Explore automated model creation**  
The RBApy package allows in theory the construction of other RBA models from existing metabolic models (of which there are many). For more senseful ecological simulations, other organisms would have been needed to be incorporated into the community simulation. Hence it was aimed to explore the model creation capability of RBApy and perhaps enhance it as well as incorporating in into the DRAMCom package.
    
5. **Build code that makes functionalities of RBApy be used more easily**
A broadly termed goal that would have included more quality of life functions like dedicated and easy visualization techniques of the results gathered from a simulation. Was only to be pursued after the completion of all the previous goals.

## Results

The package was capable of simulating the growth of both only one organism and a co-culture of two organisms. For the second case, both independent and interdependent growth were simulated and the results be seen in the graphs below.

<img src="doc/images/CMSE802_Figure_Monoculture.png" width = "40%" height = "90%">

The Monoculture simulation shows the expected result of the simplest way to model the growth of an organism on one substrate. Once the substrate runs out, the organsism (the model bacteria *Escherichia coli* in this case, abberviated as *E. coli*) ceases to grow and enters a stationary phase (Fig. 1).

<p float = "left">
    <img src="doc/images/CMSE802_Figure_Co-Culture_1.png" width = "40%" height = "40%"/>
    <img src="doc/images/CMSE802_Figure_Co-Culture_2.png" width = "40%" height = "40%"/>
</p>

The same can be observed for the independent growth of two *E. coli* types (Fig. 2). A more complex, yet not perfect result can be seen in Fig. 3 where the substrate of one organism is further produced by the other. In this case, glucose is consumed by *E. coli* wt and produced by *E. coli* CO<sub>2</sub>, and vice versa for carbon dioxide. Hence, the amount of CO<sub>2</sub> increases at first due to rapid growth and production of *E. coli* wt, but once *E. coli* CO<sub>2</sub> reaches a critical density, the consumption of carbon dioxide overtakes production and so the concentration of it decreases. The unrealistic result of this simulation is the fact that the organisms do not cease to grow/start to die once their substrate runs out, but instead keep growing. This is due to problems with perfectly integrating the RBApy core code into the package. In the future, this would be further investigated to achieve a more senseful outcome of such a simulation. Nevetheless, these simulation results show that the package works as intended in its basic form and can serve as a framework for future enhancements.

## Challenges

Several big obstacles were encountered that hampered the progress of the project. Below these are outlined and further possible solutions presented for a theoretical continuation of the project.

1. **Adjusted Feasibility**  
dFBA uses a slight modification to the FBA base LP, where additional variables are added to the optimization problem which serve only to relax the mass conservation constraint. The same was added to the package for an efficient integration process. However, there were big problems in understanding the constraint matrix creation of RBApy in such a detail that would allow their correct implementation. The adjusted feasibility was added to the package, but it took a significant amount of time to implement. In the future, such a rework should maybe be postponed if deemed to complicated, but it was also due to a lack of sufficient understanding of higher linear algebra. More knowledge about the mathematical basis of the algorithm is also critical.

2. **RBApy Metabolism and Simulations**  
The problem of the RBApy package is that it behaved differently to the COBRApy package that allows FBA computations. It cannot be explained why at this point due to the limited time available to investigate this, but dedicated metabolite export reactions where either not found or behaved in such a way that would have not allowed their implementation into DRAMCom. This is important however as therefore the basis of mutual metabolite exchange would need such reactions as well. For the simulations, constant values were assume as a means to achieve a proof of concept result, but this issue should be investigated further as another way to complete the realistic nature for DRAMCom simulations.  
As was also mentioned above, the microorganisms do not behave correctly for this reason and no fully realistic simulation result could be achieved. Moreover, all simulations were run with a very low number of timepoints as well as very high integration tolerances. The simulations would need to be rerun to fully verify their validity as well.

----
## References 

[1] Bulović, A., Fischer, S., Dinh, M., Golib, F., Liebermeister, W., Poirier, C., ... & Goelzer, A. (2019). Automated generation of bacterial resource allocation models. Metabolic engineering, 55, 12-22.  
[2] OpenStax, Biogeochemical Cycles. OpenStax CNX. 20. Juni 2013 http://cnx.org/contents/65d16444-425c-4f43-bc48-a3d0de0c2fbd@6.  
[3] Christie-Oleza, J. A., Sousoni, D., Lloyd, M., Armengaud, J., & Scanlan, D. J. (2017). Nutrient recycling facilitates long-term stability of marine microbial phototroph–heterotroph interactions. Nature microbiology, 2(9), 1-10.  
[4] Aumont, O., Éthé, C., Tagliabue, A., Bopp, L., & Gehlen, M. (2015). PISCES-v2: an ocean biogeochemical model for carbon and ecosystem studies. Geoscientific Model Development, 8(8), 2465-2513.  
[5] Fenchel, T. (2008). The microbial loop–25 years later. Journal of Experimental Marine Biology and Ecology, 366(1-2), 99-103.  
[6] McCarren, J., Becker, J. W., Repeta, D. J., Shi, Y., Young, C. R., Malmstrom, R. R., ... & DeLong, E. F. (2010). Microbial community transcriptomes reveal  microbes and metabolic pathways associated with dissolved organic matter turnover in the sea. Proceedings of the National Academy of Sciences, 107(38), 16420-16427.  
[7] Mahadevan, R., Edwards, J. S., & Doyle III, F. J. (2002). Dynamic flux balance analysis of diauxic growth
in Escherichia coli. Biophysical journal, 83(3), 1331-1340.  
[8] Feist, A. M., & Palsson, B. Ø. (2008). The growing scope of applications of genome-scale metabolic
reconstructions using Escherichia coli. Nature biotechnology, 26(6), 659-667.  
[9] Goelzer, A., Muntel, J., Chubukov, V., Jules, M., Prestel, E., Nölker, R., ... & Fromion, V. (2015). Quantitative prediction of genome-wide resource allocation in bacteria. Metabolic engineering, 32, 232-243.  
[10] Gomez, J. A., Höffner, K., & Barton, P. I. (2014). DFBAlab: a fast and reliable MATLAB code for dynamic flux balance analysis. BMC bioinformatics, 15(1), 1-10.  

----
## Content

Lastly, an overview shall be given about the content of the package.

* `Models`  
Bi-weekly model reports that deal with key topics discussed in class lectures.
* `doc`  
The documentation about the project. The initial project proposal can be found here as well as a file explaining the adjusted feasibility in brief detail.
    * `example`  
    A tutorial of how to use this package with additional data necessary to run the code in the notebook file.
    * `images`  
    Images used for the README file.
    * `docs`  
    Auto-documentation of the code
* `dramcom`  
Central code of the package.
    * `test`  
    Testing scripts for the code.
* `makefile`  
A file to automatically create other types of 'daughter' files.
* `requirements.yml`  
Requirements for an environment where the current package can run.
