{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dbb5cbb1-c1ab-42fd-bc88-a4cfdd1fa76e",
   "metadata": {},
   "source": [
    "# Adjusted Feasibility"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1b7865f-0bc1-4f16-ad26-d310e3aeb1b3",
   "metadata": {},
   "source": [
    "## Background: dFBA"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bfada5af-5d58-42c0-92f4-9413da41aab9",
   "metadata": {},
   "source": [
    "dFBA is a method of dynamically calculate metabolite levels in a time-series simulation. It is build upon the method of FBA where the stoichiometric information of a metabolic network is used to construct an optimization problem. The base linear programming problem (LP) is as follows:\n",
    "\n",
    "max $c \\cdot v$  \n",
    "s.t. $ S \\cdot v = 0$  \n",
    "$ \\:\\:\\:\\:\\:\\:  v_{i, min} \\leq v_i \\leq v_{i, max}$  \n",
    "\n",
    "where $S$ is the stoichiometric matrix, $v$ is the vector containing all reaction fluxes, and $v_{i,min}$ and $v_{i,max}$ represent lower and upper bounds respectively for each reaction flux.  \n",
    "in dFBA, the steady state output of this LP is used to create the right hand side information of a system of ordinary differential equations (ODEs). This systme is then solved with standard integration methods. Solving such a system however is more complicated than it seems, as during integration certain intricacies of the code can cause a 'false infeasibility' that would break the integration <sup>[1]</sup>. As Gomez et al. outlined in their study, the solution is to add additional variables to the LP and divide into two parts: one part calculating the lowest possible values for these additional variables, and the other one using these values as an additional constraint in the same LP as used for FBA initially <sup>[1]</sup>. The adjusted feasibility then looks as\n",
    "\n",
    "min $\\sum\\limits_{i=1}^n s_+ - s_-$  \n",
    "s.t. $S \\cdot v + s_+ - s_- = 0$  \n",
    "$  \\:\\:\\:\\:\\:\\: v_{i, min} \\leq v_i \\leq v_{i, max}$  \n",
    "$  \\:\\:\\:\\:\\:\\: s_+ \\geq 0, s_- \\geq 0$  \n",
    "\n",
    "max $c \\cdot v$  \n",
    "s.t. $ S \\cdot v + s_+ - s_- = 0$  \n",
    "$ \\:\\:\\:\\:\\:\\:  v_{i, min} \\leq v_i \\leq v_{i, max}$ \n",
    "\n",
    "with $s_+$ and $s_-$ being these additional variables. Optimally, these values would be zero, and nothing changes. However, in cases where the base LP would encounter an infeasible event, the the $s$ variables would allow a relaxation such that ther is always a feasible solution to the problem. With this, an efficient integration process is guaranteed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3c44dc5-b171-4ccf-bc71-e34ddb75b996",
   "metadata": {},
   "source": [
    "## Implementation for dRBA"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6eb4bef2-1c85-462e-bb61-5729ae378700",
   "metadata": {},
   "source": [
    "The RBA LP is a bit more complicated than the FBA one. For a complete description, see <sup>[2]</sup> and <sup>[3]</sup>. Here only the first constrained is shown, which is the only relevant one for the adjusted feasibility:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6faa5222-d801-4b79-9bb2-1ee50145a9d6",
   "metadata": {},
   "source": [
    "$ -Sv + \\mu \\left( C^S_E E + C^S_P P + C^S_{TC} TC \\right) + C^S_{TF} TF = 0 $."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a65cf654-d7b7-472f-9d3e-afc5208967d8",
   "metadata": {},
   "source": [
    "$S$ is again the stoichiometric matrix and $v$ the flux vector. Additionally, $\\mu$ is the growth rate, $C^S_E$ is the composition matrix of the amount of metabolite precursors used to produce the respectve enzymes, while $E$ is the concentration of all enzymes - the same is true for $C^S_P P$ with the case of $P$ being non-enzymatic proteins. $TF$ and $TC$ stand for target flux and target concentration respectively and are constant terms that describe certain reactions or macromolecules that need to be constantly upheld to a certain flux/concentration.  \n",
    "\n",
    "The feasibility adjustment is in principal as easy as adding variables $s_+$ and $s_-$ to this equation on the right hand side. However, RBApy does not implement the LP as equations, but rather in the standard form for linear optimization problems - matrix form. In this case, a constraint matrix is constructed using the constant information that is multiplied by a vector of variables to yield the right hand side constraints. A transformation of above equation with the added feasibility variables into standard form would be"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cddbac56-c188-46cf-b052-e2fa45acbc7e",
   "metadata": {},
   "source": [
    "|                 | Reactions | Enzymes     | Process Machinery (Ribosomes) | Targets                  | Feasbility constraints |            |\n",
    "|-----------------|-----------|-------------|-------------------------------|--------------------------|------------------------|------------|\n",
    "| Metabolite Rows | $S$       | $\\mu C^S_E$ | $\\mu CS_P$                    | $\\mu CS_{TC} + C^S_{TF}$ | $-1 ... 0$             |  $1 ... 0$ |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ff586ff-603f-4f53-8420-e01bb9cafb14",
   "metadata": {},
   "source": [
    "with the first rows corresponding to the first constraint of the constraint matrix in a table format and"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b30ea72a-c4aa-4644-b7d9-64d2039a5654",
   "metadata": {},
   "source": [
    "$ \\left( v_1 ... v_r \\:\\: E_1 ... E_e \\:\\: P_1 ... P_p \\:\\: t_1 ... t_{\\tau} \\:\\: s^+_1 ... s^+_r \\: s^-_1 ... s-_r \\right) $"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f31fd02-2cb0-4814-b15a-228207936c01",
   "metadata": {},
   "source": [
    "as the transpose of the constraint matrix.  \n",
    "This modified constraint matrix is constructed with the `RBA_ConstraintMatrix_model.py` file that is a modification to the original `constraint_matrix.py` file of RBApy. This file is used by the `solver.py` file originally to construct the full LP with the CPLEX solver suite and to compute the solution. In the modified `RBA_Solver_model.py` file, the original solving is enhanced with an `if` case that is activated once the original LP does not finda sufficient solution. In that case the constraint matrix with adjusted feasibility is constructed and at first the $s_values$ calculated. After that, these are used as constraints in the modified base LP to compute a solution. Below a prove of this is demonstrated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "00615824-5f40-4955-b54b-bd1858beb686",
   "metadata": {},
   "outputs": [],
   "source": [
    "import rba\n",
    "from RBA_ConstraintMatrix_mod import ConstraintMatrix_mod\n",
    "from RBA_Solver_mod import Solver_mod\n",
    "model = rba.RbaModel.from_xml(\"Escherichia-coli-K12-WT\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "737d48eb-c4c0-4cfd-8170-3c08357d0d2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "model.medium[\"M_glc__D\"] = 0.0005"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db78fd92-2a91-4a89-b211-ba77ede1927e",
   "metadata": {},
   "source": [
    "The basic RBApy algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "031301b7-a74e-4d36-8722-3e7d6761215d",
   "metadata": {},
   "outputs": [],
   "source": [
    "base_result = model.solve()\n",
    "base_result.mu_opt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "833b878f-6324-4a59-a6e8-e484c566c51c",
   "metadata": {},
   "source": [
    "With this, the integration cannot continue and breaks off.  \n",
    "Now with adjusted feasibility:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2563b5f-b358-499f-a63a-6728da8cd77d",
   "metadata": {},
   "outputs": [],
   "source": [
    "model_matrix = ConstraintMatrix_mod(model)\n",
    "model_matrix.build_matrices(0.08, adj = True)\n",
    "\n",
    "model_solver = Solver_mod(model_matrix)\n",
    "model_solver.solve()\n",
    "model_solver.mu_opt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "082e361d-a356-4916-8539-339c53376aea",
   "metadata": {},
   "source": [
    "This allows the integration to continue until the desired time limit."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efc3a2ed-2f12-4e07-985d-e45b13b0fb86",
   "metadata": {},
   "source": [
    "## References"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3fea17a8-69d4-4ca0-b7c1-e7d8a64026a2",
   "metadata": {},
   "source": [
    "[1] Jeanne, G., Goelzer, A., Tebbani, S., Dumur, D., & Fromion, V. (2018). Dynamical resource allocation models for bioreactor optimization. IFAC-PapersOnLine, 51(19), 20-23.  \n",
    "[2] Goelzer, A., Muntel, J., Chubukov, V., Jules, M., Prestel, E., Nölker, R., ... & Fromion, V. (2015). Quantitative prediction of genome-wide resource allocation in bacteria. Metabolic engineering, 32, 232-243.  \n",
    "[3] Biosys team, INRAE. https://rba.inrae.fr/index.html. Last day of access: 17.12.2021.  "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
